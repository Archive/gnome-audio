VERSION=2.22.2
DD=gnome-audio-$(VERSION)

all: gnome-audio.spec

gnome-audio.spec: gnome-audio.spec.in
	sed "s/GNOME_AUDIO_VERSION/$(VERSION)/" gnome-audio.spec.in > gnome-audio.spec

install:
	$(MAKE) -C sounds install

dist: gnome-audio.spec
	make datadir=$(shell pwd)/$(DD) install
	install -m644 Makefile README gnome-audio.spec.in gnome-audio.spec ChangeLog COPYING $(DD)
	install -m644 sounds/Makefile $(DD)/sounds
	tar -cjvf $(DD).tar.bz2 $(DD)
	rm -rf $(DD)

